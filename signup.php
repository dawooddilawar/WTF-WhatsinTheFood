<!DOCTYPE html>
<html>
<head>
	<title>WTF - Sign Up</title>
	<meta name="viewport" content="width=device-width"/>
	<link rel="stylesheet" type="text/css" href="bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link href='https://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="container-fluid" id="wrapper">
		<div class="row">
			<div class="col-sm-5">
				<div class="navbar">
					<ul>
						<li id="signup" class="nav-links" style="float: left;"><a href="singup.html">Sign Up</a></li>
						<li id="login" class="nav-links" style="float: left;"><a href="login.html" >Log In</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-2" id="wrapper">
				
				<div>
					<h2 class="text-front" id="su-heading">Sign Up Form</h2>
				</div>
				<form action="signup-form.php" method="POST">
					<input type="name" class="input" placeholder="Name" id="name" name="name" >
					<input type="email" class="input" placeholder="Email" id="email" name="email" >
					<input type="password" class="input" placeholder="Password" id="pass" name="pass" >
					<input type="number" class="input" id="age" placeholder="Age" name="age" >
					<input type="number" placeholder="Height(ft.)" id="h-ft" name="h-ft" >
					<input type="number" placeholder="Inches" id="h-in" name="h-in"> 
					<input type="number" class="input" id="weight" placeholder="Weight (kg)" name="weight" >
					<select class="input" id="gender" name="gender" >						
						<option value="male" selected="">Male</option>
						<option value="female">Female</option>
					</select>
					<button id="signup-btn">Sign Up</button>
				</form>
			</div>
			<div class="col-sm-5">
				
			</div>
			
		</div>
		
	</div>

</body>
</html>